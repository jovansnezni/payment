package rs.ac.bg.etf.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import rs.ac.bg.etf.payment.entity.CreditCard;
import rs.ac.bg.etf.payment.entity.User;
import rs.ac.bg.etf.payment.repository.CreditCardRepository;
import rs.ac.bg.etf.payment.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class PaymentApplication {
    @Autowired
    UserRepository userRepository;

    @Autowired
    CreditCardRepository creditCardRepository;

    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication.class, args);
    }

    @PostConstruct
    private void initData() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);

        if (users.isEmpty()) {
            User petar = new User("Petar Pan", true);
            User jovan = new User("Jovan Snezni", true);
            User alan = new User("Alan Harper", false);

            userRepository.save(petar);
            userRepository.save(jovan);
            userRepository.save(alan);


            creditCardRepository.save(new CreditCard(petar,"000-000-000",0));
            creditCardRepository.save(new CreditCard(petar,"111-111-111",111));
            creditCardRepository.save(new CreditCard(jovan,"222-222-222",222));
            creditCardRepository.save(new CreditCard(alan,"333-333-333",333));
            creditCardRepository.save(new CreditCard(alan,"444-444-444",444));
        }

    }

}
