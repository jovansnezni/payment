package rs.ac.bg.etf.payment.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.etf.payment.entity.User;
import rs.ac.bg.etf.payment.repository.CreditCardRepository;
import rs.ac.bg.etf.payment.repository.UserRepository;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CreditCardRepository creditCardRepository;


    @GetMapping("/{id}/enabled")
    public boolean isEnabled(@PathVariable int id) {
        boolean enabled = userRepository.findById(id)
                .filter(User::isEnabled)
                .isPresent();
        return enabled;
    }

    @GetMapping("/cardnumber/{number}")
    public User getUserForCreditCard(@PathVariable String number) {
        return userRepository.findByCreditCardNumber(number);
    }
}
