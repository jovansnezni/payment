# Payment

Sva potrebna konfiguracija aplikacije se nalazi unutar src/main/resources/applicayion.properties fajla.


    spring.jpa.hibernate.ddl-auto=update
	spring.datasource.url=jdbc:mysql://localhost:3306/payment?useSSL=false
	spring.datasource.username=root
	spring.datasource.password=root

	server.port=8000

Radi pokretanja aplikacije neophodno je postaviti parametre konekcije prema mysql bazi, username, root i url. 

U bazi mora da postoje shema **payment**, tabele ce biti samostalno kreirane nakon pokretanja.

Pri prvom pokretanju baza ce se napuniti test podacima, sto radi sledeca metoda:

	@PostConstruct
    private void initData() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);

        if (users.isEmpty()) {
            User petar = new User("Petar Pan", true);
            User jovan = new User("Jovan Snezni", true);
            User alan = new User("Alan Harper", false);

            userRepository.save(petar);
            userRepository.save(jovan);
            userRepository.save(alan);


            creditCardRepository.save(new CreditCard(petar,"000-000-000",0));
            creditCardRepository.save(new CreditCard(petar,"111-111-111",111));
            creditCardRepository.save(new CreditCard(jovan,"222-222-222",222));
            creditCardRepository.save(new CreditCard(alan,"333-333-333",333));
            creditCardRepository.save(new CreditCard(alan,"444-444-444",444));
        }

    }


