package rs.ac.bg.etf.payment.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import rs.ac.bg.etf.payment.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("select cc.user from CreditCard cc where cc.cardNumber =:cardNumber")
    User findByCreditCardNumber(@Param("cardNumber") String cardNumber);
}
