package rs.ac.bg.etf.payment.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.etf.payment.entity.CreditCard;
import rs.ac.bg.etf.payment.repository.CreditCardRepository;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController {
    @Autowired
    CreditCardRepository creditCardRepository;

    @PutMapping("/{cardnumber}/{amount}")
    public void withdrawalMoney(@PathVariable String cardnumber, @PathVariable int amount) {
        CreditCard cc = creditCardRepository.findByCreditCardNumber(cardnumber);

        cc.setAmount(cc.getAmount() - amount);
        creditCardRepository.save(cc);
    }

}
