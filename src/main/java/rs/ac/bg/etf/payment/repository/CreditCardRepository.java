package rs.ac.bg.etf.payment.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import rs.ac.bg.etf.payment.entity.CreditCard;

public interface CreditCardRepository extends CrudRepository<CreditCard, Integer> {

    @Query("select cc from CreditCard cc where cc.cardNumber =:cardNumber")
    CreditCard findByCreditCardNumber(@Param("cardNumber") String cardNumber);
}
